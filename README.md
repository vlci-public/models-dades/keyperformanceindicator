# KeyPerformanceIndicator

Models de dades del projecte VLCi basats en els data models Fiware: https://github.com/smart-data-models/dataModel.KeyPerformanceIndicator

Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/keyperformanceindicator/-/blob/main/spec.md) que se tiene. Contiene el modelo generico KPI utilizado por vlci.

Otras especificaciones históricas
===
Entidades de los subservicios Turismo [Especificación Turismo](https://gitlab.com/vlci-public/models-dades/keyperformanceindicator/-/blob/main/archive/spec-turismo.md)