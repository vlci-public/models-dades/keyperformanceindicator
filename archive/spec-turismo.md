Entidad: KeyPerformanceIndicator 
===========================
  

[Licencia abierta](https://github.com/smart-data-models/dataModel.KeyPerformanceIndicator/blob/master/keyPerformanceIndicator/LICENSE.md)  

Descripción global: **Los indicadores KPI se utilizan para representar mediciones de rendimiento.**  


## Lista de propiedades  

- `calculationFrequency`: Con qué frecuencia se calcula el KPI. Posibles valores: horario, diario, semanal, mensual, anual, trimestral, bimestral, quincenal,etc.
- `calculationFrequencyId`: Identificador numérico del atributo calculationFrequency.
- `calculationPeriod`: Periodo temporal en el que se ha realizado la medición.
- `category`: Categoría de indicador. Posibles valores: quantitative, qualitative, leading, lagging, etc.
- `definitionOwner`: 
- `description`: Descripción de esta entidad.
- `desiredTrend`: 
- `id`: Identificador único de la entidad.
- `internationalStandard`: 
- `isKPIFraction`: 
- `kpiDenominatorValue`: Número denominador utilizado para la obtención del valor del KPI.
- `kpiInternalIdentifier`: Identificador interno del indicador en forma de texto.
- `kpiNumeratorValue`: Número numerador utilizado para la obtención del valor del KPI.
- `kpiType`: Tipo del KPI.
- `kpiValue`: Valor del indicador. Puede ser de cualquier tipo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `lowerThreshold`: 
- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `measureUnit`: Nombre de la unidad de medida.
- `measureUnitId`: Id de la unidad de medida.
- `name`: El nombre de este artículo.
- `neighborhoodId`: Id del barrio.
- `observations`: 
- `operationalStatus`: Posibles valores: ok, noData.
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `organization`: Organización que evalua el KPI.
- `processName`: 
- `product`: Hay que definir el proceso o el producto.
- `provider`: Proveedor del producto o servicio, si lo hay, que este KPI evalúa.
- `references`: 
- `serviceId`: Id del servicio.
- `serviceName`: Nombre del servicio.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.
- `sliceAndDice1`: Primer criterio de desagregación.
- `sliceAndDice2`: Segundo criterio de desagregación.
- `sliceAndDice3`: Tercer criterio de desagregación.
- `sliceAndDice4`: Cuarto criterio de desagregación.
- `sliceAndDiceValue1`: Valor del primer criterio de desagregación.
- `sliceAndDiceValue2`: Valor del segundo criterio de desagregación.
- `sliceAndDiceValue3`: Valor del tercer criterio de desagregación.
- `sliceAndDiceValue4`: Valor del cuarto criterio de desagregación.
- `smartDimension`: 
- `source`: Una secuencia de caracteres que proporciona la fuente original de los datos de la entidad.
- `systemType`: 
- `technology`: 
- `type`: NGSI Tipo de entidad.
- `updatedAt`: Fecha y hora en la que se actualizó el valor de la entidad.  
- `upperThreshold`: 

## Atributos utilizados para CdmCiutat (EMT,Trafico...)
- `diaSemana`: Indica el día de la semana de la ultima medición. Posibles valores: L,M,X,J,V,S,D.

## Atributos utilizados para Residuos 
- `calculatedBy`: Indica que entidad realiza el cálculo.
- `calculationFormula`:
- `calculationMethod`: Indica de que forma se realiza el cálculo (Ej: Manual).
- `denominatorValue`: Equivalente a kpiDenominatorValue.
- `numeratorValue`: Equivalente a kpiNumeratorValue.
- `process`: Equivalente a processName.

## Lista de entidades que implementan este modelo (4 entidades)
| Subservicio    | ID Entidad                   |
|----------------|------------------------------|
| /turismo       | IS.TUR.001                   |
| /turismo       | ...                          |
| /turismo       | IS.TUR.004                   |