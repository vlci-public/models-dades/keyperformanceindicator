Entidad: KeyPerformanceIndicator 
===========================
  

[Licencia abierta](https://github.com/smart-data-models/dataModel.KeyPerformanceIndicator/blob/master/keyPerformanceIndicator/LICENSE.md)  

Descripción global: **Los indicadores KPI se utilizan para representar mediciones de rendimiento.**  

Utilizar los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

Todos los textos deben usar el Valenciano. 

# Lista de propiedades obligatorias
### Identificación de la entidad
- `id`: Identificador único de la entidad.
- `type`: NGSI Tipo de entidad. (En este caso KeyPerformanceIndicator)
### Atributos que se definen y se les da valor una única vez
- `calculationFrequency`: Con qué frecuencia se calcula el KPI. Posibles valores: horari, diari, semanal, mensual, anual, trimestral, bimestral, semestral, quincenal, etc. (En valenciano)
- `definitionOwner`: Responsable de la definición del indicador
- `description`: Descripción de esta entidad. (En valenciano)
- `isKPIFraction`: true / false. True indica que el KPI se obtiene en base a un numerador y un denominador. False indica que el KPI es un valor concreto, que no necesita de una división para calcularse. Cuando este campo sea true, obligatoriamente se tiene que tener valor en los campos kpiNumeratorValue y kpiDenominatorValue.
- `kpiDenominatorDescription`: Descripción de qué mide el numerador utilizado para la obtención del valor del KPI.  Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio. (En valenciano)
- `kpiNumeratorDescription`: Descripción de qué mide el numerador utilizado para la obtención del valor del KPI.  Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio. (En valenciano)
- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `measureUnit`: Nombre de la unidad de medida. (En valenciano)
- `name`: El nombre de este artículo. (En valenciano)
- `organization`: Organización que evalua el KPI.
- `project`: Proyecto (para el proyecto impulso su valor es impulsoVLCI-red.es)
- `serviceName`: Nombre del servicio. (En valenciano)
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.
- `sliceAndDice1`: Primer criterio de desagregación. Puede ser vacío, pero el atributo debe existir. (En valenciano)
- `sliceAndDice2`: Segundo criterio de desagregación. Puede ser vacío, pero el atributo debe existir. (En valenciano)
- `sliceAndDice3`: Tercer criterio de desagregación. Puede ser vacío, pero el atributo debe existir. (En valenciano)
- `sliceAndDice4`: Cuarto criterio de desagregación. Puede ser vacío, pero el atributo debe existir. (En valenciano)
### Atributos que varían en cada envío
- `calculationPeriod`: Periodo temporal en el que se ha realizado la medición. En otras palabras, dado una calculationFrequency = anual, el valor concreto, ej: 2022. A continuación se lista el formato esperado con ejemplos para cada uno de los calculationFrequency:

| calculationFrequency | Formato                                          | Ejemplo              | Comentario                                                                      |
| -------------------- | ------------------------------------------------ | -------------------- | ------------------------------------------------------------------------------- |
| cinc minutal         | ISO 8601 (Fecha y hora, extendida, con TimeZone) | 2024-07-18T09:15:00Z | Representa la hora de finalización del KPI                                      |
| horari               | ISO 8601 (Fecha y hora, extendida, con TimeZone) | 2023-05-02T14:00:00Z | Representa la hora de finalización del KPI                                      |
| diari                | AAAA-MM-DD                                       | 2025-12-23           |                                                                                 |
| semanal              | AAAA-Www                                         | 2024-W12             | Se considera la primera semana de un año (semana W01) aquella que contiene el primer jueves de dicho año, o lo que es lo mismo, aquella que contiene el día 4 de enero. |
| quincenal            | AAAA-MM-Qx                                       | 2024-10-Q1           | x puede valer 1, si es la 1a quincena del mes o 2, si es la 2a quincena del mes |
| mensual              | AAAA-MM                                          | 2022-08              |                                                                                 |
| bimestral            | AAAA-Bx                                          | 2025-B3              | x puede tomar valores entre 1 y 6, para los 6 bimestres del año                 |
| trimestral           | AAAA-Tx                                          | 2025-T2              | x puede tomar valores entre 1 y 4, para los 4 trimestres del año                |
| semestral            | AAAA-Sx                                          | 2025-S1              | x puede tomar valores entre 1 y 2, para los 2 semestres del año                 |
| anual                | AAAA                                             | 2025                 |                                                                                 |

- `kpiDenominatorValue`: Número denominador utilizado para la obtención del valor del KPI. Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio. (Si tiene decimales, utilizar el . como separador decimal)
- `kpiNumeratorValue`: Número numerador utilizado para la obtención del valor del KPI.  Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio. (Si tiene decimales, utilizar el . como separador decimal)
- `kpiValue`: Valor del indicador. Puede ser de cualquier tipo. (Si tiene decimales, utilizar el . como separador decimal)
- `operationalStatus`: Posibles valores: ok, noData. Funcionamiento normal: en cada envío establecer ese atributo a ok. Internamente, la plataforma de ciudad establecerá el valor noData en caso de pasar un X tiempo sin recibir datos del indicador.
- `sliceAndDiceValue1`: Valor del primer criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir. (En valenciano)
- `sliceAndDiceValue2`: Valor del segundo criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir. (En valenciano)
- `sliceAndDiceValue3`: Valor del tercer criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir. (En valenciano)
- `sliceAndDiceValue4`: Valor del cuarto criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir. (En valenciano)

# Lista de propiedades opcionales
- `calculationFormula`:  (En valenciano)
- `category`: Categoría de indicador. Posibles valores: quantitative, qualitative, leading, lagging, etc. (En valenciano)
- `address`: La dirección donde ubicar el KPI.
- `postalCode`: El código postal donde ubicar el KPI.
- `province`: La provincia donde ubicar el KPI.
- `municipality`: El municipio donde ubicar el KPI.
- `censusTract`: Sección censal
- `censusTractId`: Identificador de Sección censal
- `desiredTrend`: 
- `district`: Distrito.  (En valenciano). [Ver el apéndice 1](#apéndice-listado-de-distritos)
- `districtId`: Id del distrito. [Ver el apéndice 1](#apéndice-listado-de-distritos)
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `lowerThreshold`: Umbral inferior (Si tiene decimales, utilizar el . como separador decimal)
- `neighborhood`: Barrio. (En valenciano). [Ver el apéndice 2](#apéndice-listado-de-barrios)
- `neighborhoodId`: Id del barrio. [Ver el apéndice 2](#apéndice-listado-de-barrios)
- `observations`:  (En valenciano)
- `processName`: 
- `product`: Hay que definir el proceso o el producto.
- `provider`: Proveedor del producto o servicio, si lo hay, que este KPI evalúa.
- `references`: 
- `smartDimension`: 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen. (En valenciano)
- `systemType`: 
- `technology`: 
- `upperThreshold`: Umbral superior (Si tiene decimales, utilizar el . como separador decimal)

## Atributos Multiidioma (Si se tienen, publicarlos)
- `calculationFormulaCas`:
- `calculationFrequencyCas`: Con qué frecuencia se calcula el KPI. Posibles valores: horario, diario, semanal, mensual, anual, trimestral, bimestral, quincenal,etc. El idioma es castellano.
- `descriptionCas`: Descripción de esta entidad en castellano.
- `measureUnitCas`: Unidad de medida en castellano.
- `nameCas`: El nombre de este artículo en castellano.
- `sliceAndDice1Cas`: Primer criterio de desagregación en castellano.
- `sliceAndDice2Cas`: Segundo criterio de desagregación en castellano.
- `sliceAndDice3Cas`: Tercer criterio de desagregación en castellano.
- `sliceAndDice4Cas`: Cuarto criterio de desagregación en castellano.
- `districtCas`: Distrito en castellano.
- `neighborhoodCas`: Barrio en castellano.
- `sliceAndDiceValue1Cas`: Valor del primer criterio de desagregación en castellano.
- `sliceAndDiceValue2Cas`: Valor del segundo criterio de desagregación en castellano.
- `sliceAndDiceValue3Cas`:Valor del tercer criterio de desagregación en castellano.
- `sliceAndDiceValue4Cas`: Valor del cuarto criterio de desagregación en castellano.
- `sourceCas`: Una secuencia de caracteres que proporciona la fuente original de los datos de la entidad en castellano.

## Atributos que de la plataforma, que requieren de desarrollo en plataforma para que se generen
- `TimeInstant`: Fecha y hora en la que se actualizó el valor de la entidad.

## Atributos que establece la Oficina Técnica de Proyectos, de uso interno
- `calculationFrequencyId`: Identificador numérico del atributo calculationFrequency. 
- `kpiInternalIdentifier`: Identificador interno del indicador en forma de texto. Lo establece el equipo de la OTP.
- `kpiType`: Tipo del KPI. Posibles valores: Servicio o Ciudad. Lo establece el equipo de la OTP
- `internationalStandard`: ISO, ITU, etc.
- `measureUnitId`: Id de la unidad de medida.
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `serviceId`: Id del servicio.

## Atributos específicos de ciertos desarrollos

### Atributos utilizados para CdmCiutat (EMT,Trafico...)
- `diaSemana`: Indica el día de la semana de la ultima medición. Posibles valores: L,M,X,J,V,S,D.

### Atributos utilizados para Valencia al Minut (EMT,Trafico...)
- `nameShort`: El nombre de este artículo pero más abreviado. Este es utilizado para mostrar el nombre de las leyendas de las gráficas cuando las visualizamos en formato móvil.

### Atributos utilizados para Residuos 
- `calculatedBy`: Indica que entidad realiza el cálculo.
- `calculationMethod`: Indica de que forma se realiza el cálculo (Ej: Manual).
- `denominatorValue`: Equivalente a kpiDenominatorValue.
- `numeratorValue`: Equivalente a kpiNumeratorValue.
- `process`: Equivalente a processName.

### Atributos utilizados para Estadística
- `numberIndicator`: Número de Indicadores.
- `numberMeta`: Número de Metas.
- `numberOds`: Número de Ods.
- `observations1Cas`: Observaciones en castellano.
- `observations2Cas`: Observaciones en castellano.

### Atributos utilizados para Tráfico
- `isAggregated`: Indica si el valor esta agregado o no. Posibles valores: S o N.
- `measurelandUnit`: Equivalente a measureUnit.
- `owner`: Equivalente a serviceOwner.
- `ownerEmail`: Equivalente a serviceOwnerEmail.
- `tramo`: Descripcion del tramo.

# Apéndice Listado de Distritos

| id distrito | Distrito                                  |
| :---------: | :---------------------------------------- |
| 1 | CIUTAT VELLA |
| 2 | L'EIXAMPLE |
| 3 | EXTRAMURS |
| 4 | CAMPANAR |
| 5 | LA SAIDIA |
| 6 | EL PLA DEL REAL |
| 7 | L'OLIVERETA |
| 8 | PATRAIX |
| 9 | JESUS |
| 10 | QUATRE CARRERES |
| 11 | POBLATS MARITIMS |
| 12 | CAMINS AL GRAU |
| 13 | ALGIROS |
| 14 | BENIMACLET |
| 15 | RASCANYA |
| 16 | BENICALAP |
| 17 | POBLATS DEL NORD |
| 18 | POBLATS DE L'OEST |
| 19 | POBLATS DEL SUD |


# Apéndice Listado de Barrios

| id distrito | id barrio | Barrio                                |
| :---------: | :-------: | :------------------------------------ |
| 1 | 1.1 | LA SEU |
| 1 | 1.2 | LA XEREA |
| 1 | 1.3 | EL CARME |
| 1 | 1.4 | EL PILAR |
| 1 | 1.5 | EL MERCAT |
| 1 | 1.6 | SANT FRANCESC |
| 2 | 2.1 | RUSSAFA |
| 2 | 2.2 | EL PLA DEL REMEI |
| 2 | 2.3 | GRAN VIA |
| 3 | 3.1 | EL BOTÀNIC |
| 3 | 3.2 | LA ROQUETA |
| 3 | 3.3 | LA PETXINA |
| 3 | 3.4 | ARRANCAPINS |
| 4 | 4.1 | CAMPANAR |
| 4 | 4.2 | LES TENDETES |
| 4 | 4.3 | EL CALVARI |
| 4 | 4.4 | SANT PAU |
| 5 | 5.1 | MARXALENES |
| 5 | 5.2 | MORVEDRE |
| 5 | 5.3 | TRINITAT |
| 5 | 5.4 | TORMOS |
| 5 | 5.5 | SANT ANTONI |
| 6 | 6.1 | EXPOSICIÓ |
| 6 | 6.2 | MESTALLA |
| 6 | 6.3 | JAUME ROIG |
| 6 | 6.4 | CIUTAT UNIVERSITÀRIA |
| 7 | 7.1 | NOU MOLES |
| 7 | 7.2 | SOTERNES |
| 7 | 7.3 | TRES FORQUES |
| 7 | 7.4 | LA FONTSANTA |
| 7 | 7.5 | LA LLUM |
| 8 | 8.1 | PATRAIX |
| 8 | 8.2 | SANT ISIDRE |
| 8 | 8.3 | VARA DE QUART |
| 8 | 8.4 | SAFRANAR |
| 8 | 8.5 | FAVARA |
| 9 | 9.1 | LA RAIOSA |
| 9 | 9.2 | L'HORT DE SENABRE |
| 9 | 9.3 | LA CREU COBERTA |
| 9 | 9.4 | SANT MARCEL·LÍ |
| 9 | 9.5 | CAMÍ REAL |
| 10 | 10.1 | MONT-OLIVET |
| 10 | 10.2 | EN CORTS |
| 10 | 10.3 | MALILLA |
| 10 | 10.4 | FONTETA DE SANT LLUÍS |
| 10 | 10.5 | NA ROVELLA |
| 10 | 10.6 | LA PUNTA |
| 10 | 10.7 | CIUTAT DE LES ARTS I DE LES CIÈNCIES |
| 11 | 11.0 | EL PUERTO |
| 11 | 11.1 | EL GRAU |
| 11 | 11.2 | EL CABANYAL-EL CANYAMELAR |
| 11 | 11.3 | LA MALVA-ROSA |
| 11 | 11.4 | BETERÓ |
| 11 | 11.5 | NATZARET |
| 11 | 11.6 | LES MORERES |
| 12 | 12.1 | AIORA |
| 12 | 12.2 | ALBORS |
| 12 | 12.3 | LA CREU DEL GRAU |
| 12 | 12.4 | CAMÍ FONDO |
| 12 | 12.5 | PENYA-ROJA |
| 13 | 13.1 | L'ILLA PERDUDA |
| 13 | 13.2 | CIUTAT JARDÍ |
| 13 | 13.3 | L'AMISTAT |
| 13 | 13.4 | LA BEGA BAIXA |
| 13 | 13.5 | LA CARRASCA |
| 14 | 14.1 | BENIMACLET |
| 14 | 14.2 | CAMÍ DE VERA |
| 15 | 15.1 | ORRIOLS |
| 15 | 15.2 | TORREFIEL |
| 15 | 15.3 | SANT LLORENÇ |
| 16 | 16.1 | BENICALAP |
| 16 | 16.2 | CIUTAT FALLERA |
| 17 | 17.1 | BENIFARAIG |
| 17 | 17.2 | POBLE NOU |
| 17 | 17.3 | CARPESA |
| 17 | 17.4 | CASES DE BÀRCENA |
| 17 | 17.5 | MAUELLA |
| 17 | 17.6 | MASSARROJOS |
| 17 | 17.7 | BORBOTÓ |
| 18 | 18.1 | BENIMÀMET |
| 18 | 18.2 | BENIFERRI |
| 19 | 19.1 | EL FORN D'ALCEDO |
| 19 | 19.2 | EL CASTELLAR-L'OLIVERAR |
| 19 | 19.3 | PINEDO |
| 19 | 19.4 | EL SALER |
| 19 | 19.5 | EL PALMAR |
| 19 | 19.6 | EL PERELLONET |
| 19 | 19.7 | LA TORRE |
| 19 | 19.8 | FAITANAR |
